﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestImageOperators
{
    [AttributeUsage(AttributeTargets.Method)]
    public class OperationCategory :  Attribute
    {
        public OperationCategory() { }

        public string Category { get; set; }
    }
}
