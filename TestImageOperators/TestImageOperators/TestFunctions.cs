﻿using System;
using System.Collections.Generic;
using InterfaceImageOperators;
using OpenCvSharp;

namespace TestImageOperators
{
    public class TestFunctions
    {

        IImageOperators imageOperators;
        public TestFunctions(IImageOperators imageOperators)
        {
            this.imageOperators = imageOperators;
        }

        /// <summary>
        /// Compares all properties of two Mat objects
        /// </summary>
        /// <param name="cvMat">OpenCV Mat object</param>
        /// <param name="ownMat">Mat object from our own implementation</param>
        private Mat CompareMats(Mat cvMat, Mat ownMat)
        {
            Mat dstMat = new Mat(cvMat.Height, cvMat.Width, MatType.CV_8UC3);

            if (cvMat.Type() == ownMat.Type())
            {
                Cv2.Absdiff(cvMat, ownMat, dstMat);

                if (dstMat.Channels() != 1)
                    Cv2.CvtColor(dstMat, dstMat, ColorConversionCodes.BGR2GRAY);
            }

            return dstMat;
        }

        /// <summary>
        /// Returns the results of testing the Clear method with a value
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestClearWithValueByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat cvClear = original.Clone();
            cvClear.SetTo(new Scalar(TestValues.Value, TestValues.Value, TestValues.Value));

            Mat ownClear = imageOperators.ClearImage(original, TestValues.Value);

            Mat CompareMat = CompareMats(cvClear, ownClear);

            return new TestResults
            {
                CvMat = cvClear,
                OwnMat = ownClear,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Clear to zero method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestClearToZeroByComparingOpenCV(Mat original)
        {
            Mat cvClear = original.Clone();
            cvClear.SetTo(new Scalar(0, 0, 0));

            Mat ownClear = imageOperators.ClearImage(original);

            Mat CompareMat = CompareMats(cvClear, ownClear);

            return new TestResults
            {
                CvMat = cvClear,
                OwnMat = ownClear,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Copy method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestCopyByComparingOpenCV(Mat original)
        {
            Mat cvCopy = new Mat();
            original.CopyTo(cvCopy);

            Mat ownCopy = imageOperators.CopyImage(original);

            Mat CompareMat = CompareMats(cvCopy, ownCopy);

            return new TestResults
            {
                CvMat = cvCopy,
                OwnMat = ownCopy,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Invert method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestInvertByComparingOpenCV(Mat original)
        {
            Mat cvInverted = new Mat();
            Cv2.BitwiseNot(original, cvInverted);

            Mat ownInverted = imageOperators.InvertImage(original);
            Mat CompareMat = CompareMats(cvInverted, ownInverted);

            return new TestResults
            {
                CvMat = cvInverted,
                OwnMat = ownInverted,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Shift method with direction parameter
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestShifWithDirectionByComparingOpenCV(Mat original)
        {
            Mat cvTranslated = new Mat();
            float[] data = new float[] { 1, 0, 10, 0, 1, 20 };
            Mat matrix = new Mat(2, 3, MatType.CV_32FC1, data);
            Cv2.WarpAffine(original, cvTranslated, matrix, new Size(original.Width, original.Height));

            Mat ownTranslated = imageOperators.ShiftImage(original, DirectionsEnum.BottomRight, 10, 20);

            Mat CompareMat = CompareMats(cvTranslated, ownTranslated);

            return new TestResults
            {
                CvMat = cvTranslated,
                OwnMat = ownTranslated,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Shift method with XY steps
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestShifWithXYStepsByComparingOpenCV(Mat original)
        {
            Mat cvTranslated = new Mat();
            float[] data = new float[] { 1, 0, -15, 0, 1, 40 };
            Mat matrix = new Mat(2, 3, MatType.CV_32FC1, data);
            Cv2.WarpAffine(original, cvTranslated, matrix, new Size(original.Width, original.Height));

            Mat ownTranslated = imageOperators.ShiftImage(original, -15, 40);

            Mat CompareMat = CompareMats(cvTranslated, ownTranslated);

            return new TestResults
            {
                CvMat = cvTranslated,
                OwnMat = ownTranslated,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Shift method with translation matrix
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestShifWithMatrixByComparingOpenCV(Mat original)
        {
            Mat cvTranslated = new Mat();
            float[] data = new float[] { 1, 0, 100, 0, 1, 200 };
            Mat matrix = new Mat(2, 3, MatType.CV_32FC1, data);
            Cv2.WarpAffine(original, cvTranslated, matrix, new Size(original.Width, original.Height));

            Mat ownTranslated = imageOperators.ShiftImage(original, matrix);

            Mat CompareMat = CompareMats(cvTranslated, ownTranslated);

            return new TestResults
            {
                CvMat = cvTranslated,
                OwnMat = ownTranslated,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Add constant method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestAddConstantByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat cvAddedConstant = original.Clone();
            // cvAddedConstant.ConvertTo(cvAddedConstant, -1, 1, 100);
            cvAddedConstant.ConvertTo(cvAddedConstant, -1, 1, 0.1f);
            Mat ownAddedConstant = imageOperators.AddConstant(original, 0.1f);

            Mat CompareMat = CompareMats(cvAddedConstant, ownAddedConstant);

            return new TestResults
            {
                CvMat = cvAddedConstant,
                OwnMat = ownAddedConstant,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Scaling method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "All")]
        public TestResults TestScaleByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat cvScaled = original.Clone();
            cvScaled.ConvertTo(cvScaled, -1, 2, TestValues.Value);
            Mat ownScaled = imageOperators.Scaling(original, 2, TestValues.Value);

            Mat CompareMat = CompareMats(cvScaled, ownScaled);

            return new TestResults
            {
                CvMat = cvScaled,
                OwnMat = ownScaled,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }


        /// <summary>
        /// Returns the results of testing the Threshold method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Grayscale")]
        public TestResults TestThresholdByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat cvThresholded = new Mat();
            Cv2.Threshold(original, cvThresholded, TestValues.ThresholdValue, TestValues.ThresholdMaxValue, ThresholdTypes.Binary);

            Mat ownThresholded = imageOperators.VisualiseBinary(imageOperators.ThresholdImage(original, TestValues.ThresholdValue));
            
            if (original.Type() == MatType.CV_32FC1)
            {
                cvThresholded.ConvertTo(cvThresholded, MatType.CV_8UC1, 255.0);
            }

            Mat CompareMat = CompareMats(cvThresholded, ownThresholded);

            return new TestResults
            {
                CvMat = cvThresholded,
                OwnMat = ownThresholded,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Threshold method with lower and upper limits
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Grayscale")]
        public TestResults TestThresholdRangeByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat myThresholdedInRange = imageOperators.VisualiseBinary(imageOperators.ThresholdImage(original, TestValues.ThresholdValue, TestValues.ThresholdUpperValue));
            Mat cvThreshInRange = new Mat();

            // In range execute operation: lowerThresh <= P0 <= upperThresh
            // Our implementation executes: lowerThresh < P0 && < upperThresh. That's why 101 and 149 instead of 100 and 150

            Cv2.InRange(original, TestValues.ThresholdValue + TestValues.RangeConstant, TestValues.ThresholdUpperValue - TestValues.RangeConstant, cvThreshInRange);

            if (original.Type() == MatType.CV_32FC1)
            {
                myThresholdedInRange.ConvertTo(myThresholdedInRange, MatType.CV_8UC1, 255.0);
            }
            else if (original.Type() == MatType.CV_16UC1)
            {
                cvThreshInRange.ConvertTo(cvThreshInRange, MatType.CV_16UC1, 257);
            }

            Mat CompareMat = CompareMats(myThresholdedInRange, cvThreshInRange);

            return new TestResults
            {
                CvMat = cvThreshInRange,
                OwnMat = myThresholdedInRange,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Threshold method with lower and upper limits and option to invert it
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Grayscale")]
        public TestResults TestThresholdRangeAndInvertByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat myThresholdedRangeInv = imageOperators.VisualiseBinary(imageOperators.ThresholdImage(original, TestValues.ThresholdValue, TestValues.ThresholdUpperValue, true));
            Mat cvThreshRangeInv = new Mat();

            // In range execute operation: lowerThresh <= P0 <= upperThresh
            // Our implementation executes: lowerThresh < P0 && < upperThresh. That's why 101 and 149 instead of 100 and 150

            Cv2.InRange(original, TestValues.ThresholdValue + TestValues.RangeConstant, TestValues.ThresholdUpperValue - TestValues.RangeConstant, cvThreshRangeInv);
            
            if (original.Type() == MatType.CV_32FC1)
            {
                myThresholdedRangeInv.ConvertTo(myThresholdedRangeInv, MatType.CV_8UC1, 255.0);
            }
            else if (original.Type() == MatType.CV_16UC1)
            {
                cvThreshRangeInv.ConvertTo(cvThreshRangeInv, MatType.CV_16UC1, 257);
            }

            Cv2.BitwiseNot(cvThreshRangeInv, cvThreshRangeInv);

            Mat CompareMat = CompareMats(myThresholdedRangeInv, cvThreshRangeInv);

            return new TestResults
            {
                CvMat = cvThreshRangeInv,
                OwnMat = myThresholdedRangeInv,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Threshold method with lower and upper limits, option to invert it or change background intensity
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Grayscale")]
        public TestResults TestThresholdRangeInvertAndKeepValByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat myThresholdedRangeInvKeep = imageOperators.VisualiseBinary(imageOperators.ThresholdImage(original, TestValues.ThresholdValue, TestValues.ThresholdMaxValue + TestValues.RangeConstant, false, -1));
            Mat cvThreshRangeInvKeep = new Mat();

            Cv2.Threshold(original, cvThreshRangeInvKeep, TestValues.ThresholdValue, TestValues.ThresholdUpperValue, ThresholdTypes.Tozero);
            Mat CompareMat = CompareMats(myThresholdedRangeInvKeep, cvThreshRangeInvKeep);

            return new TestResults
            {
                CvMat = cvThreshRangeInvKeep,
                OwnMat = myThresholdedRangeInvKeep,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the VisualizeBinary method (Binary)
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestVisualizeBinaryByComparingOpenCV(Mat original)
        {
            TestValues.SetValues(original.Type());
            Mat myVisualizeBin = imageOperators.VisualiseBinary(original);
            Mat cvVisualizeBinv = new Mat();
            Cv2.Threshold(imageOperators.VisualiseBinary(original), cvVisualizeBinv, TestValues.ThresholdValue, TestValues.ThresholdMaxValue, ThresholdTypes.Binary);

            Mat CompareMat = CompareMats(myVisualizeBin, cvVisualizeBinv);

            return new TestResults
            {
                CvMat = cvVisualizeBinv,
                OwnMat = myVisualizeBin,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Erosion method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestErosionByComparingOpenCV(Mat original)
        {
            Mat cvEroded = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.Erode(imageOperators.VisualiseBinary(original), cvEroded, kernel, iterations: 1);

            Mat ownEroded = imageOperators.VisualiseBinary(imageOperators.Erosion(original));

            Mat CompareMat = CompareMats(cvEroded, ownEroded);

            return new TestResults
            {
                CvMat = cvEroded,
                OwnMat = ownEroded,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }


        /// <summary>
        /// Returns the results of testing the dilation method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestDilationByComparingOpenCV(Mat original)
        {
            Mat cvDilated = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.Dilate(imageOperators.VisualiseBinary(original), cvDilated, kernel, iterations: 1);

            Mat ownDilated = imageOperators.VisualiseBinary(imageOperators.Dilation(original));

            Mat CompareMat = CompareMats(cvDilated, ownDilated);

            return new TestResults
            {
                CvMat = cvDilated,
                OwnMat = ownDilated,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };

        }


        /// <summary>
        /// Returns the results of testing the binary boundary detection method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestFindBinaryBoundaryByComparingOpenCV(Mat original)
        {
            Mat cvEdgeDetected = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.MorphologyEx(imageOperators.VisualiseBinary(original), cvEdgeDetected, MorphTypes.Gradient, kernel);

            Mat ownEdgeDetected = imageOperators.VisualiseBinary(imageOperators.BinaryBoundary(original));

            Mat CompareMat = CompareMats(cvEdgeDetected, ownEdgeDetected);

            return new TestResults
            {
                CvMat = cvEdgeDetected,
                OwnMat = ownEdgeDetected,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Salt noise removal method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestRemoveSaltNoiseByComparingOpenCV(Mat original)
        {
            Mat cvSaltRemoved = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.MorphologyEx(imageOperators.VisualiseBinary(original), cvSaltRemoved, MorphTypes.Open, kernel);

            Mat ownSaltRemoved = imageOperators.VisualiseBinary(imageOperators.RemoveSaltNoise(original));

            Mat CompareMat = CompareMats(cvSaltRemoved, ownSaltRemoved);

            return new TestResults
            {
                CvMat = cvSaltRemoved,
                OwnMat = ownSaltRemoved,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }


        /// <summary>
        /// Returns the results of testing the Salt noise removal method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestRemovePepperNoiseByComparingOpenCV(Mat original)
        {
            Mat cvPepperRemoved = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.MorphologyEx(imageOperators.VisualiseBinary(original), cvPepperRemoved, MorphTypes.Close, kernel);

            Mat ownPepperRemoved = imageOperators.VisualiseBinary(imageOperators.RemovePepperNoise(original));

            Mat CompareMat = CompareMats(cvPepperRemoved, ownPepperRemoved);

            return new TestResults
            {
                CvMat = cvPepperRemoved,
                OwnMat = ownPepperRemoved,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }


        /// <summary>
        /// Returns the results of testing the Salt and pepper noise removal method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestRemoveSaltPepperNoiseByComparingOpenCV(Mat original)
        {
            Mat cvRemoveNoise = new Mat();
            Cv2.MedianBlur(imageOperators.VisualiseBinary(original), cvRemoveNoise, 3);
            Mat ownRemoveNoise = imageOperators.VisualiseBinary(imageOperators.RemoveNoise(original));

            Mat CompareMat = CompareMats(cvRemoveNoise, ownRemoveNoise);

            return new TestResults
            {
                CvMat = cvRemoveNoise,
                OwnMat = ownRemoveNoise,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing the Basic convolution method
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Grayscale")]
        public TestResults TestApplyBasicConvolutionByComparingOpenCV(Mat original)
        {
            Mat cvConvolution = new Mat();
            int kernelSize = 3;
            Mat kernel = Mat.Ones(kernelSize, kernelSize, MatType.CV_32FC1);
            kernel = kernel / (float)(kernelSize * kernelSize);
            Cv2.Filter2D(imageOperators.VisualiseBinary(original), cvConvolution, -1, kernel);
            Mat ownConvolution = imageOperators.VisualiseBinary(imageOperators.BasicConvolutionImage(original, kernel));

            Mat CompareMat = CompareMats(cvConvolution, ownConvolution);

            return new TestResults
            {
                CvMat = cvConvolution,
                OwnMat = ownConvolution,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing Erosion followed by Dilation 
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestOpeningByComparingOpenCV(Mat original)
        {
            Mat cvOpening = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.MorphologyEx(imageOperators.VisualiseBinary(original), cvOpening, MorphTypes.Open, kernel);

            Mat ownOpening = imageOperators.Erosion(original);
            ownOpening = imageOperators.VisualiseBinary(imageOperators.Dilation(ownOpening));

            Mat CompareMat = CompareMats(cvOpening, ownOpening);

            return new TestResults
            {
                CvMat = cvOpening,
                OwnMat = ownOpening,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results of testing Dilation followed by Erosion 
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestClosingByComparingOpenCV(Mat original)
        {
            Mat cvClosing = new Mat();
            Mat kernel = Mat.Ones(new Size(3, 3), MatType.CV_8U);
            Cv2.MorphologyEx(imageOperators.VisualiseBinary(original), cvClosing, MorphTypes.Close, kernel);

            Mat ownClosing = imageOperators.Dilation(original);
            ownClosing = imageOperators.VisualiseBinary(imageOperators.Erosion(ownClosing));

            Mat CompareMat = CompareMats(cvClosing, ownClosing);

            return new TestResults
            {
                CvMat = cvClosing,
                OwnMat = ownClosing,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }

        /// <summary>
        /// Returns the results first book problem
        /// Computer Vision Principles Algorithms Applications Learning Chapter 2.
        /// </summary>
        /// <param name="original">Original image to run the test with</param>
        [OperationCategory(Category = "Binary")]
        public TestResults TestFindBinaryBoundary(Mat original)
        {
            Mat ownErotion = imageOperators.Erosion(original);
            Mat ownDiffBinary = imageOperators.VisualiseBinary(imageOperators.DiffBinary(original, ownErotion));
            Mat ownBinaryBoundary = imageOperators.VisualiseBinary(imageOperators.BinaryBoundary(original));

            Mat CompareMat = CompareMats(ownDiffBinary, ownBinaryBoundary);

            return new TestResults
            {
                CvMat = ownDiffBinary,
                OwnMat = ownBinaryBoundary,
                EqualImages = CompareMat.CountNonZero() <= 0,
                DiffMat = CompareMat
            };
        }



    }
}