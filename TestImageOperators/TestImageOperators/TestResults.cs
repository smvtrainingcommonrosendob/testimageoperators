﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace TestImageOperators
{
    public class TestResults
    {
        /// <summary>
        /// Returns an OpenCV Mat object
        /// </summary>
        public Mat CvMat { get; set; }

        /// <summary>
        /// Returns a Mat object from our own implementation
        /// </summary>
        public Mat OwnMat { get; set; }

        /// <summary>
        /// Returns True if both Mat objects are equals. Otherwise, returns False
        /// </summary>
        public bool EqualImages { get; set; }

        /// <summary>
        /// Returns a Mat objects that represents the differences between CvMat and OwnMat
        /// </summary>
        public Mat DiffMat { get; set; }
    }
}
