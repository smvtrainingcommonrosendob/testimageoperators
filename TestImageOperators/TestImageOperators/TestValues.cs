﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace TestImageOperators
{
    public static class TestValues
    {
       
        public static float Value { get; set; }  //Add constant ,Scaling  and Clear values
        public static float ThresholdValue { get; set; }
        public static float ThresholdUpperValue { get; set; }
        public static float ThresholdMaxValue { get; set; }
        public static float RangeConstant { get; set;  }

        public static void SetValues(MatType imgType)
        {
            if (imgType == MatType.CV_8UC3 || imgType == MatType.CV_8UC1)
            {
                Value = 80;
                ThresholdValue = 100;
                ThresholdUpperValue = 150;
                ThresholdMaxValue = 255;
                RangeConstant = 1;
            }
            else if (imgType == MatType.CV_16UC3 || imgType == MatType.CV_16UC1)
            {
                Value = 8000;
                ThresholdValue = 4500;
                ThresholdUpperValue = 4800;
                ThresholdMaxValue = 65535;
                RangeConstant = 1;
            }
            else if (imgType == MatType.CV_32FC1 || imgType == MatType.CV_32FC3)
            {
                Value = 0.5f;
                ThresholdValue = 0.5f;
                ThresholdUpperValue = 0.8f;
                ThresholdMaxValue = 1;
                RangeConstant = 0.0f;
            }
        }
    }
}
